![Build Status](https://gitlab.com/pages/mkdocs/badges/master/pipeline.svg)

---

Smiley's Docusaurus website using GitLab Pages.

---

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
image: node:9.11.1

pages:
  script:
    - cd website
    - yarn install
    - yarn build
    # The build directory is created based on the value set for projectName in
    # website/siteConfig.js. If you change it there, you need to change it here
    # as well.
    - mv ./build/docusaurus ../public
  artifacts:
    paths:
      - public
  only:
    - master
```
