---
title: New Draft Commands
---
## New Pact Dice Drafting
New/alternate Pact Dice drafting is explained [here](https://docs.google.com/document/d/14-pD2sUtauQIsO4wXxzHXJXmDs-8K0B3SSaNsB7FKTA/edit).
It involves several squares of cards which get divided between players.

The `%ad` (**a**lternate **d**raft) command handles generating and running the new style of Pact Dice drafting. Use `%ad help` to get a list of subcommands (run with `%ad <subcommand>`), and `%ad help <subcommand>` to learn more about each option.

The first commands you'll want to know about are `%ad addme` and `%ad lesgo`. Before beginning a draft, all players should use `%ad addme` to join the draft. Then, `%ad lesgo` will begin the draft and generate the squares of cards in a spreadsheet.

Upon starting a draft, the bot should instruct you on how to proceed. Each round, players will DM Smiley to request a row or column from a square of cards with `%ad gib`.

### Examples:

Bid for the first row of the Puissance square:
```
%ad gib puissance 1
```
Bid for the second column of the Puissance square:
```
%ad gib puissance b
```
Bid for the fifth row of the Family square:
```
%ad gib puissance 5
```

Then, clashes between players may have to be resolved. This repeats until the draft is complete, when each player has 5 cards from each square.
