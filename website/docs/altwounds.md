---
title: 3.0 Wounds
---

In the WIP 3.0 ruleset for Weaver Dice, wounds have a type and a severity.

Severities:
 - Lesser
 - Moderate
 - Critical

Wound types:
 - Bash
 - Cut
 - Pierce
 - Burn
 - Shock
 - Rend

To roll a 3.0 wound effect of a specific effect and severity, use the `%altwound` command.
The arguments for this command are severity and type, in that order.

### Examples:
Roll a lesser cut:
```
%altwound lesser cut
```
Roll a moderate shock:
```
%altwound moderate shock
```
