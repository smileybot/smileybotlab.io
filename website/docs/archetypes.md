---
title: Archetypes
---

The `%archetypes` command can be used to generate 'archetypes': stat spreads used in some games to generate characters. By default, the command will give you 3 options. A number from 1 to 7 can be given to the command as an argument if you'd like to generate a different number of archetypes.

### Examples:

Roll three random archetypes:
```
%archetypes
```
Roll five archetypes:
```
%archetypes 5
```
