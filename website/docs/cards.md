---
title: Cards
---
## Standard deck
Draw a random card from a standard deck. You can only draw one card at a time, and it's always from a full deck.

```
%card
```

## Tarot deck
Draw a random tarot card. You can choose any tarot card, a minor arcana, or a major arcana.
You can only draw one card at a time, and it's always from a full deck.

```
%tarot
%tarot minor
%tarot major
```

## Shuffle
Shuffle a set of space-delimited arguments. Smiley will spit them back out in a random order.

```
%shuffle Choice1 Choice2
%shuffle A B C D E F
%shuffle KingOfHearts QueenOfClubs AceOfSpades
```
