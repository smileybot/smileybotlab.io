---
title: Contribute
---

## To Smiley

The bot is free and open source software, and we welcome contributions. Smiley's source is hosted on GitLab [here](https://gitlab.com/NickReu/Smiley/). You can make a merge request to Smiley with any code improvements, bug fixes, or feature additions that you write.


## To this site

This site's source code is also hosted [on GitLab](https://gitlab.com/smileybot/smileybot.gitlab.io). Merge requests are welcome.

### How to

The pages of this site are markdown text documents (.md files) under `website/docs`. You can edit any existing file to add content to a page. To add a new page, add a new .md file and add it to the sidebar at `website/sidebars.json`. The structure there should be straightforward.
