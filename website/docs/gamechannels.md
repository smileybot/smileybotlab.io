---
title: Game Channel Access
---

## Joining and leaving
To access various WD and PD game channels, the key commands are `%enter` and `%exit`. You can also use special keywords to enter or exit groups of channels.

### Examples:
Enter the channel for Pact Dice practice drafts:
```
%enter drafts
```
Enter the channel for WD Willets:
```
%enter willets
```
Leave the channel for PD Lake Pewawe:
```
%exit lake_pewawe
```
Enter all game channels:
```
%enter all
```
Enter all game channels for active WD games:
```
%enter allwd
%enter wdall
```
Enter all channels for active games:
```
%enter allactive
```
Exit all channels for archived (inactive) games:
```
%exit archived
%exit inactive
```

## Learning about game channels

You can see a list of all games with the `%campaigns` command, which will show you a link to a document with all games and some details about them. You can also find out the GM for a specific channel by using the `%owner` command.
### Example:
Find out who owns the `#outlands` channel:
```
%owner outlands
```

## A channel for your game

You can add a channel for your game with the `%addgame` command.

### Examples:
Add a Weaver Dice game set in Longmont:
```
%addgame wd longmont
```
Add a Pact Dice game set on Krakatoa:
```
%addgame pd krakatoa
```

You can also manage your channel with a few commands. You can pin messages with the `%pin` command, and put a link in the channel topic with the `%link` command. `%pin` can only be run in your game's channel, and if the `%link` command is run from your game's channel, you don't have to specify what channel to affect.

### Examples:
Pin a post (you can edit the post later to have whatever contents you like):
```
%pin Hi! This message will be pinned!
```
Add a link to the topic of the `#harrow` channel that links to a document with game details:
```
%link harrow https://docs.google.com/document/d/1VB9ZKScs5grBbmC3KL5jUlb0s_8IipJV4OUjSLAQapM/edit?usp=sharing
```
