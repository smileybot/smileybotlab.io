---
title: Start Here
slug: /
---

## Smiley

Smiley, or \[^_^\], is a bot on the [Official Parahumans Discord](https://discord.gg/Nduef2HnNE), a fan community for J. C. McCrae (AKA Wildbow)'s fiction and surrounding projects. But you probably knew that already.

Smiley has functions to facilitate the workings of the server, as well as for playing the Pact Dice and Weaver Dice RPGs.

The bot is free and open source software, and we welcome contributions. Smiley's source is hosted on GitLab [here](https://gitlab.com/NickReu/Smiley/). You're also free to try and adapt the code to use on your own server, though it will be a difficult undertaking, since it wasn't designed with that in mind.

## Using commands

You can interact with Smiley by using commands prefixed with the % (percent) character.
Most of Smiley's commands work both in private messages with Smiley or in any channel that Smiley is a member of.
Commands are case-insensitive, but their arguments (anything after the first space) may not be.
Smiley doesn't look for edits, so if you make a typo in your command, you'll have to send it again.

The best and simplest command is `%hi`.

For a list of commands, use `%help`.

You can get more help for most commands by typing `%help` followed by the name of the command.

### Examples:
Get help for rolling dice:
```
%help roll
```
Get help for Smiley's snacks:
```
%help snack
```
Get help for trading cards:
```
%help tc
```
