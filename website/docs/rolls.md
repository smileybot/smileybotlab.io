---
title: Rolling Dice
---
You can roll dice with Smiley.

## Basic rolling
The basic rolling syntax is `%roll <N>d<S> [Comment]`, where `<N>` (1-50) is the number of dice to roll, `<S>` (1-1000) is the number of sides each die has, and `[Comment]` is an optional comment for the roll that doesn't affect the result.
Smiley will roll a random number for every die, equally weighted, and will also display the total sum of results. If you leave the number of dice out, Smiley will assume you want one.

### Examples:
```
%roll d6
%roll 1d6 Social roll
%roll 4d20
%roll 3d6
```

## Modifiers
Any dice roll can have `+<C>` or `-<C>` to add or subtract a constant from the final sum.

If this constant should instead be added to each individual die, you can add an exclamation point.

### Examples:
```
%roll 4d20-2
%roll 3d6+10 Money
%roll 4d12-1!
```

## Highest and lowest
If, instead of summing the dice results, you want to choose the highest or lowest roll, you can use `%roll <N>h<S>` or `%roll <N>l<S>`.

### Examples:
```
%roll 2h20
%roll 3l6+1
```

## A bunch of dice
You can repeat the full roll by adding `x<T>` where `<T>` (1-10) is the number of times to repeat the rolls.

### Examples:
```
%roll 2h20x5
%roll 3l6+1x8
```
