---
title: Shifty, a Smiley for other servers
---

## What is it?

[<_<] (aka Shifty) is also a Discord bot. It handles dice rolling and Weaver Dice wound rolling, and has a few other features. Shifty is a trimmed-down version of Smiley. Shifty's source is hosted on GitLab [here](https://gitlab.com/NickReu/shifty). The benefit of Shifty being stripped down is that it can be invited to other servers without any hassle.

## How do I use it?

You can invite it directly to your server! 
See instructions [here](https://gitlab.com/NickReu/shifty/-/blob/main/README.md)!
