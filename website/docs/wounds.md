---
title: Wounds
---
You can roll Weaver Dice wound effects with Smiley.

There are three severities that a wound can have:
 - Lesser
 - Moderate
 - Critical

There are also six wound types:
 - Bash
 - Cut
 - Pierce
 - Burn
 - Shock
 - Rend

And finally, there are four locations that a character can be wounded in:
 - Leg
 - Torso
 - Arm
 - Head

To roll a wound with Smiley, you have two options. You can specify just the first two of the above, or all three.

If you don't specify the location, it's determined randomly, with Torso being chosen three times more than each other option.

The first two can be specified in any order, so the base command can either be the severity or the wound type.

### Examples:
Roll a lesser bash on a random location:
```
%lesser bash
%bash lesser
```
Roll a moderate rend for the leg:
```
%moderate rend leg
%rend moderate leg
```
