module.exports={
  "title": "Smiley Docs",
  "tagline": "More thorough than the %help command",
  "url": "https://smileybot.gitlab.io",
  "baseUrl": "/",
  "organizationName": "smileybot",
  "projectName": "SmileyDocs",
  "scripts": [
    "https://buttons.github.io/buttons.js"
  ],
  "favicon": "img/favicon.ico",
  "customFields": {},
  "onBrokenLinks": "error",
  "onBrokenMarkdownLinks": "error",
  "presets": [
    [
      "@docusaurus/preset-classic",
      {
        "docs": {
          "showLastUpdateAuthor": true,
          "showLastUpdateTime": true,
          "path": "docs",
          "sidebarPath": "sidebars.json"
        },
        "blog": {
          "path": "blog"
        },
        "theme": {
          "customCss": "../src/css/customTheme.css"
        }
      }
    ]
  ],
  "plugins": [],
  "themeConfig": {
    "sidebarCollapsible": false,
    "navbar": {
      "title": "Smiley Docs",
      "logo": {
        "src": "img/Smiley.png"
      },
      "items": [
        {
          "to": "docs/",
          "label": "Docs",
          "position": "left"
        },
        {
          "to": "blog/",
          "label": "Blog",
          "position": "left"
        },
      ]
    },
    "image": "img/Smiley.png",
    "footer": {
      "links": [
        {
          title: 'Links',
          items: [
            {
              label: 'Parahumans Discord',
              to: 'https://discord.gg/Nduef2HnNE',
            },
            {
              label: 'Smiley\'s Source',
              to: 'https://gitlab.com/NickReu/Smiley/',
            },
          ],
        },
      ],
      "copyright": "© 2020 Nick Auer",
    }
  }
}
